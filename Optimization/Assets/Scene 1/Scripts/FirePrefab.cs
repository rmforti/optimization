﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;
	Camera cam;

	private void Awake()
	{
		cam = GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update()
    {
		//If the button correspondent to Fire1 Input is pressed
        if ( Input.GetButton( "Fire1" ) )
        {
			//Gets the camera component, and calculates the screentoworld position of the mouse and sums the foward verctor
			//Once that is done, it calculates the direction vector that will be fired, and normalize it.
			//Once that is over, instantiates the prefab in this objects position and with identity rotation
			//Gets the rigidibody component of the prefab instance that was just made and sets the velocity to be Firedirection * firespeed
            Vector3 clickPoint = cam.ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }

}
