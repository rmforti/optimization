﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
	Renderer rendererComponent;

	private void Awake()
	{
		rendererComponent = GetComponent<Renderer>();
	}
	// Use this for initialization
	void Start()
    {
		//Gets the component rendered in children and sets the color of the material to a random color between 0,1f in red, green and blue
        rendererComponent.material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
    }

}
