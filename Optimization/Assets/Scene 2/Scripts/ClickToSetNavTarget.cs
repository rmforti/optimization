﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
	NavMeshAgent agent;
	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
	}
	// Update is called once per frame
	void Update ()
    {
		//Gets the camera every frame by finding the amin camera and getting the component camera on it
		//Casts a raycasthit, and if that raycast from the mouse position relative to camera returns a hit, with maximum range, targeting the layer ground
		//It gets the navmesh that was hit and assignts its destinatino to the hit point
        RaycastHit hit = new RaycastHit();
        if ( Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out hit, 15, 256 ) )
        {
            agent.destination = hit.point;
        }
	}
}
