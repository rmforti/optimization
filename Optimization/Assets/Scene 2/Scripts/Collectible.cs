﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
		////On update, it creates an array of colliders and calculates if they overlap with the collectible's physics sphere by passing the position and radius of it
		////Then for each collision, and looping through all the possible collisions, it tries to find a collider with the tag of a Player
		////if it returns true, it destroys the object
  //      Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
  //      for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
  //      {
  //          if ( collidingColliders[colliderIndex].tag == "Player" )
  //          {
  //              Destroy( this.gameObject );
  //          }
  //      }
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			Destroy(gameObject);
		}
	}
}
